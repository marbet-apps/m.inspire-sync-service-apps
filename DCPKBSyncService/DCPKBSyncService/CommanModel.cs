﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DCPKBSyncService
{
    public class CommanModel
    {
        /// <summary>
        /// Represents Http Status code like 200,401
        /// </summary>
        [JsonProperty(Order = 1, PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Data output
        /// </summary>
        [JsonProperty(Order = 5, PropertyName = "data", NullValueHandling = NullValueHandling.Ignore)]
        public object Data { get; set; }

        /// <summary>
        /// Success
        /// </summary>
        [JsonProperty(Order = 2, PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Message will be filled incase of Error/Fail
        /// </summary>
        [JsonProperty(Order = 3, PropertyName = "message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }
    }
    public class ResponseSyncModel
    {

        public int ResourceId { get; set; }

        public bool IsSyncSuccess { get; set; }

        public string ResourceType { get; set; }
    }
}
