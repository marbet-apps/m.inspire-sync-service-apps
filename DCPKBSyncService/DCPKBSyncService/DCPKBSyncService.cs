﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
//using System.Timers;
using System.Threading;
using DCPKBSyncBAL;
using System.IO;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using DCPKBSyncUtility;

namespace DCPKBSyncService
{
    public partial class DCPKBSyncService : ServiceBase
    {
        //Timer timer = new Timer();
         private Timer Schedular;
 
        public DCPKBSyncService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.ScheduleService();
            //timer.Interval = 5000;
            //timer.Enabled = true;
        }

        protected override void OnStop()
        {

        }
        public void ScheduleService()
        {
            clsMigration migration = new clsMigration();
            try
            {
                Common.Token = Common.GetBasicAuthenticationString();
                Schedular = new Timer(new TimerCallback(SchedularCallback));
                string mode = ConfigurationSettings.AppSettings["Mode"].ToUpper();
                Log.WriteToFile("Start DCP sync service: " +DateTime.Now.ToString());

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                //if (mode == "DAILY")
                //{
                //    //Get the Scheduled Time from AppSettings.
                //    scheduledTime = DateTime.Parse(ConfigurationSettings.AppSettings["ScheduledTime"]);
                //    if (DateTime.Now > scheduledTime)
                //    {
                //        //If Scheduled Time is passed set Schedule for the next day.
                //        scheduledTime = scheduledTime.AddDays(1);
                //    }
                //}

                if (mode.ToUpper() == "INTERVAL")
                {
                    //Get the Interval in Minutes from AppSettings.
                    int intervalMinutes = Convert.ToInt32(ConfigurationSettings.AppSettings["IntervalMinutes"]);

                    //Set the Scheduled Time by adding the Interval to Current Time.
                    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next Interval.
                        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                    }
                }

                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                //string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                //this.WriteToFile("Simple Service scheduled to run after: " + schedule + " {0}");
               
                //Thread.Sleep(1000);
                string jsonData = clsConvertToJson.ConvertToJson(migration.GetMasterList());
                migration.DeleteKbResource();
                string HotelJsonData = clsConvertToJson.ConvertToJson(migration.GetHotelResourceList());
               // this.WriteToFile(jsonData);
               // this.WriteToFile(HotelJsonData);
                bool isHotelApi = SyncKBtoDCPAsync(HotelJsonData, "H");
                if(isHotelApi == false)
                {
                    Log.WriteToFile("Not able to start Hotel API " + DateTime.Now.ToString());
                }
                string LocationJsonData = clsConvertToJson.ConvertToJson(migration.GetLocationResourceList());
                //this.WriteToFile(LocationJsonData);
                bool isLocationAPI = SyncKBtoDCPAsync(LocationJsonData, "L");
                if(isLocationAPI == false)
                {
                    Log.WriteToFile("Not able to start Location API " + DateTime.Now.ToString());
                }
                string RestaurantJsonData = clsConvertToJson.ConvertToJson(migration.GetRestaurantResourceList());
                //this.WriteToFile(RestaurantJsonData);

               bool isRestAPI = SyncKBtoDCPAsync(RestaurantJsonData, "R");
                if(isRestAPI == false)
                {
                    Log.WriteToFile("Not able to start Restaurant API " + DateTime.Now.ToString());
                }
                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {

                 Log.WriteToLogFile("ScheduleService", ex);
               // WriteToFile("Service Error on "+ DateTime.Now.ToString() + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }
        private  bool SyncKBtoDCPAsync(string requestparameter,string ResourceType)
        {
            clsMigration clsMigration = new clsMigration();
            try
            {
               
                HttpClient client = new HttpClient();
                string apiURL = "";
                string APIBaseURL= ConfigurationSettings.AppSettings["APIURL"].ToString();
                if (ResourceType == "H")
                {
                    Log.WriteToFile("Started Hotel API: " + DateTime.Now.ToString());
                    apiURL = "api/sync/synchotel";
                }
                if (ResourceType == "L")
                {
                    Log.WriteToFile("Started Location API: " + DateTime.Now.ToString());
                    apiURL = "api/sync/synclocation";

                }
                if (ResourceType == "R")
                {
                    Log.WriteToFile("Started restaurant API: " + DateTime.Now.ToString());
                    apiURL = "api/sync/syncrestaurant";
                }
                client.BaseAddress = new Uri(APIBaseURL);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Common.Token);
                var requestResponse = client.PostAsync(apiURL, GetHttpContent(requestparameter)).Result;
                if (requestResponse.IsSuccessStatusCode)
                {
                    string responseResult = requestResponse.Content.ReadAsStringAsync().Result;
                    CommanModel responseSyncModels = new CommanModel();
                    responseSyncModels = JsonConvert.DeserializeObject<CommanModel>(responseResult);
                    if (responseSyncModels.Data != null)
                    {
                        Log.WriteToFile("API response successfully. " + DateTime.Now.ToString());

                        List<ResponseSyncModel> responseSyncs = JsonConvert.DeserializeObject<List<ResponseSyncModel>>(responseSyncModels.Data.ToString());

                        foreach (var item in responseSyncs)
                        {
                            if (item.IsSyncSuccess == true)
                            {
                                clsMigration.UpdateResourceStatus(item.ResourceId, item.ResourceType);
                            }
                            else
                            {
                               Log.WriteToFile("API Update failed for Id: " + item.ResourceId + " for resource type" + item.ResourceType +" on " + DateTime.Now.ToString());
                            }
                        }
                    }
                    return true;
                }
                else
                {
                   Log.WriteToFile("API response failed. " + DateTime.Now.ToString());
                   Log.WriteToFile("API parameter. " + requestparameter);

                    return false;
                }
            }
            catch(Exception ex)
            {
                Log.WriteToLogFile("SyncKBtoDCPAsync", ex);
                return false;
            }            
        }


        private HttpContent GetHttpContent(string json)
        {
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private void SchedularCallback(object e)
        {
            //this.WriteToFile("Simple Service Log: {0}");
            this.ScheduleService();
        }
        //public void WriteToLogFile(string methodName, string message, string description)
        //{
        //    string strMessage = string.Empty;
        //    strMessage = Environment.NewLine;
        //    strMessage += "----------------------------------------------------";
        //    strMessage += Environment.NewLine;
        //    strMessage += "Date : " + DateTime.Now.ToString();
        //    strMessage += Environment.NewLine;
        //    strMessage += "Method : " + methodName;
        //    strMessage += Environment.NewLine;
        //    strMessage += "Message : " + message;
        //    if(!string.IsNullOrEmpty(description))
        //    {
        //        strMessage += Environment.NewLine;
        //        strMessage += "StackTrace : " + description;
        //    }
        //    WriteToFile(strMessage);
        //}

        //public void WriteToFile(string Message)
        //{
        //    string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
        //    if (!Directory.Exists(path))
        //    {
        //        Directory.CreateDirectory(path);
        //    }
        //    string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
        //    if (!File.Exists(filepath))
        //    {
        //        // Create a file to write to.   
        //        using (StreamWriter sw = File.CreateText(filepath))
        //        {
        //            sw.WriteLine(Message);
        //        }
        //    }
        //    else
        //    {
        //        using (StreamWriter sw = File.AppendText(filepath))
        //        {
        //            sw.WriteLine(Message);
        //        }
        //    }
        //}
    }
}
