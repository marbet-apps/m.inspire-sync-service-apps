﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

//#if DEBUG
//            DCPKBSyncService service = new DCPKBSyncService();
//            service.ScheduleService();
//            service.Stop();

//#else
             ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new DCPKBSyncService()
            };
            ServiceBase.Run(ServicesToRun);

//#endif



        }
    }
}
