﻿namespace DCPKBSyncService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DCPKBSyncProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.DCPKBSyncServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // DCPKBSyncProcessInstaller
            // 
            this.DCPKBSyncProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.DCPKBSyncProcessInstaller.Password = null;
            this.DCPKBSyncProcessInstaller.Username = null;
            // 
            // DCPKBSyncServiceInstaller
            // 
            this.DCPKBSyncServiceInstaller.Description = "DCP KB SyncService";
            this.DCPKBSyncServiceInstaller.DisplayName = "DCPKBSyncService";
            this.DCPKBSyncServiceInstaller.ServiceName = "DCPKBSyncService";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DCPKBSyncProcessInstaller,
            this.DCPKBSyncServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller DCPKBSyncProcessInstaller;
        private System.ServiceProcess.ServiceInstaller DCPKBSyncServiceInstaller;
    }
}