﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncUtility
{
    public class Log
    {
        public static string AppLogFile { get => ConfigurationSettings.AppSettings["AppLogFile"].ToString(); }
        public static void WriteToLogFile(string methodName, Exception ex = null)
        {
            string strMessage = string.Empty;
            strMessage = Environment.NewLine;
            strMessage += "----------------------------------------------------";
            strMessage += Environment.NewLine;
            strMessage += "Date         : " + DateTime.Now.ToString();
            strMessage += Environment.NewLine;
            strMessage += "Method       : " + methodName;
            strMessage += Environment.NewLine;
            strMessage += "Message      : " + ex.Message;

            if (ex != null && ex.InnerException != null && ex.InnerException.Message != null)
            {
                strMessage += Environment.NewLine;
                strMessage += "InnerExceptionMessage    : " + ex.InnerException.Message;
            }

            if (ex != null && ex.InnerException != null && ex.InnerException.InnerException != null && ex.InnerException.InnerException.Message != null)
            {
                strMessage += Environment.NewLine;
                strMessage += "InnerToInnerExceptionMessage : " + ex.InnerException.InnerException.Message;
            }

            if (!string.IsNullOrEmpty(ex.StackTrace))
            {
                strMessage += Environment.NewLine;
                strMessage += "StackTrace : " + ex.StackTrace;
            }

            WriteToFile(strMessage);
        }

        public static void WriteToFile(string Message)
        {            
            // string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            string path = AppLogFile + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppLogFile + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
