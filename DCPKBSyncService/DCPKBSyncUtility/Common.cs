﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncUtility
{
    public class Common
    {
        private static string username = ConfigurationSettings.AppSettings["UserName"].ToString();
        private static string password = ConfigurationSettings.AppSettings["Password"].ToString();

        public static string Token { get; set; }

        public static string GetBasicAuthenticationString()
        {
            string input = string.Format("{0}{1}", username, password);            
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("abmw-8hn9-sqoy20");
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);            
        }
    }
}
