﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class KitchenModel
    {
        public int id { get; set; }

        public string kitchen_name { get; set; }

        public string kitchen_name_de { get; set; }
    }
}
