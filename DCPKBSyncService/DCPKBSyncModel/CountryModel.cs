﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class CountryModel
    {
        public int Id { get; set; }

        public string country_name { get; set; }

        public string country_name_de { get; set; }

        public string country_code { get; set; }
    }
}
