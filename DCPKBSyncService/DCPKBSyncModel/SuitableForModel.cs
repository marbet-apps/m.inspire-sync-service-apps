﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class SuitableForModel
    {
        public int id { get; set; }

        public string suitablefor_name { get; set; }

        public string suitablefor_name_de { get; set; }

        public int suitablefor_type { get; set; }
    }
}
