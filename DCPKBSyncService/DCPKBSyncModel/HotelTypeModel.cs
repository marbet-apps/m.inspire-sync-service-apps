﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class HotelTypeModel
    {
        public int Id { get; set; }

        public string hoteltype_name { get; set; }

        public string hoteltype_name_de { get; set; }
    }
}
