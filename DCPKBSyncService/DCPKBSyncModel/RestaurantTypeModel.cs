﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class RestaurantTypeModel
    {
        public int id { get; set; }

        public string restaurant_name { get; set; }

        public string restaurant_name_de { get; set; }
    }
}
