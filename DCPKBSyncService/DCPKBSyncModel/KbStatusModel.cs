﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class KbStatusModel
    {
        public int id { get; set; }

        public int kb_id { get; set; }

        public int kb_type { get; set; }

        public string dcp_status { get; set; }

        public string update_from { get; set; }
    }
}
