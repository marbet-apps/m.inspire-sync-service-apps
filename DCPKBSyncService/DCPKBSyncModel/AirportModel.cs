﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPKBSyncModel
{
    public class AirportModel
    {
        public int airport_id { get; set; }

        public string airport_name { get; set; }

        public int? country_id { get; set; }

        public string state { get; set; }

        public string city { get; set; }

        public string zipcode { get; set; }

        public double? airport_lan { get; set; }

        public double? airport_lat { get; set; }

    }
}
