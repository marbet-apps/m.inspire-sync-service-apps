﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCPKBSyncDAL;
using System.Data;
using Newtonsoft.Json;
using System.Security;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using Newtonsoft.Json;
using DCPKBSyncModel;
using System.IO;
using DCPKBSyncUtility;

namespace DCPKBSyncBAL
{
    public class clsMigration
    {
        ClsBase ClsBase;
        public string MarbetConnectionString { get => ConfigurationSettings.AppSettings["MarbetConnectionString"].ToString(); }

       // public string DCPConnectionstring { get => ConfigurationSettings.AppSettings["DCPConnectionString"].ToString(); }


        public string APIUrl {
            get {
                return ConfigurationSettings.AppSettings["APIURL"].ToString();
            }
        }

        public string Token { get; set; }

        List<SqlParameter> sp = new List<SqlParameter>();

        public DataSet GetMasterList()
        {
            DataSet _dataset = new DataSet();
            DataSet _DCPdataset = new DataSet();
            DataSet dsDifferences = new DataSet();
            try
            {
                ClsBase = new ClsBase();
                _dataset = ClsBase.GetMasterList("DCP_Migration_Master", MarbetConnectionString);
               // _DCPdataset = ClsBase.GetMasterList("DCP_Migration_Master_Select", DCPConnectionstring);
                CustomDataRowComparer myDRComparer = new CustomDataRowComparer();
                MigrateAirport(_dataset.Tables[0]);
                MigrateCountry(_dataset.Tables[1]);
                MigrateHotelType(_dataset.Tables[2]);
                MigrateRestaurantType(_dataset.Tables[3]);
                MigrateListLocationType(_dataset.Tables[4]);
                MigrateKitchen(_dataset.Tables[5]);
                MigrateSuitableFor(_dataset.Tables[6]);
            }
            catch (Exception ex)
            {                
                Log.WriteToLogFile("GetMasterList", ex);
            }
            return _dataset;
        }

        public DataSet GetHotelResourceList()
        {
            DataSet _dataset = new DataSet();
            try
            {
                ClsBase = new ClsBase();
                _dataset = ClsBase.GetResourceList("DCP_Migration_Hotel", MarbetConnectionString, "Hotels");
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("GetHotelResourceList", ex);
            }
            return _dataset;
        }

        public DataSet GetLocationResourceList()
        {
            DataSet _dataset = new DataSet();
            try
            {
                ClsBase = new ClsBase();
                _dataset = ClsBase.GetResourceList("DCP_Migration_Location", MarbetConnectionString, "Locations");
            }
            catch (Exception ex)
            {
               Log.WriteToLogFile("GetLocationResourceList", ex);
            }
            return _dataset;
        }

        public DataSet GetRestaurantResourceList()
        {
            DataSet _dataset = new DataSet();
            try
            {
                ClsBase = new ClsBase();
                _dataset = ClsBase.GetResourceList("DCP_Migration_Restaurant", MarbetConnectionString, "Restaurants");
            }
            catch (Exception ex)
            {
               Log.WriteToLogFile("GetRestaurantResourceList", ex);
            }
            return _dataset;
        }

        public void MigrateKitchen(DataTable SourceTable)
        {
            try
            {
                IEnumerable<KitchenModel> sourceList = SourceTable.AsEnumerable().Select(row => new KitchenModel()
                {
                    id = Convert.ToInt32(row["smt_kitchen_id"]),
                    kitchen_name = Convert.ToString(row["kitchen_name"]),
                    kitchen_name_de = Convert.ToString(row["kitchen_name_de"]),

                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migratekitchen", sourceList).Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("MigrateKitchen", ex);
            }

            

            //var result = SourceTable.AsEnumerable().Except(DestinationTable.AsEnumerable(), new CustomDataRowComparer());
            //var result2 = result.Any() ? result.CopyToDataTable() : null;
            //if (result2 != null && result2.Rows.Count > 0)
            //{
            //    foreach (DataRow item in result2.Rows)
            //    {
            //        sp = new List<SqlParameter>()
            //            {
            //                new SqlParameter() {ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value= item.ItemArray[0]},
            //                new SqlParameter() {ParameterName = "@kitchen_name", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[1]},
            //                new SqlParameter() {ParameterName = "@kitchen_name_de", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[2]}
            //            };
            //        ClsBase.SyncMasterList("DCP_Migration_List_Kitchen", DCPConnectionstring, sp);
            //    }
            //}
        }

        public void MigrateCountry(DataTable SourceTable)
        {
            try
            {
                IEnumerable<CountryModel> sourceList = SourceTable.AsEnumerable().Select(row => new CountryModel()
                {
                    Id = Convert.ToInt32(row["Id"]),
                    country_name = Convert.ToString(row["Name"]),
                    country_name_de = Convert.ToString(row["Name_de"]),
                    country_code = Convert.ToString(row["Google_description"]),

                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migratecountry", sourceList).Result;
                    
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
               Log.WriteToLogFile("MigrateCountry", ex);
            }  
        }

        public void MigrateAirport(DataTable SourceTable)
        {
            try
            {
                IEnumerable<AirportModel> sourceList = SourceTable.AsEnumerable().Select(row => new AirportModel()
                {
                    airport_id = Convert.ToInt32(row["airport_id"]),
                    airport_name = Convert.ToString(row["airport_name"]),
                    country_id = row["country_id"].ToString() == "" ? null : (int?)row["country_id"],
                    city = Convert.ToString(row["city"]),
                    state = row["state"].ToString() == "" ? null : Convert.ToString(row["state"]),
                    zipcode = Convert.ToString(row["zipcode"]),
                    airport_lan = row["airport_lan"].ToString() == "" ? null : (double?)row["airport_lan"],
                    airport_lat = row["airport_lat"].ToString() == "" ? null : (double?)row["airport_lat"]
                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migrateairport", sourceList).Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("MigrateAirport", ex);
            }
        }

        public void MigrateHotelType(DataTable SourceTable)
        {
            try
            {
                IEnumerable<HotelTypeModel> sourceList = SourceTable.AsEnumerable().Select(row => new HotelTypeModel()
                {
                    Id = Convert.ToInt32(row["smt_hoteltype_id"]),
                    hoteltype_name = Convert.ToString(row["hoteltype_name"]),
                    hoteltype_name_de = Convert.ToString(row["hoteltype_name_de"]),

                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migratehoteltype", sourceList).Result;
                    
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("MigrateHotelType", ex);
            }

           

            //var result = SourceTable.AsEnumerable().Except(DestinationTable.AsEnumerable(), new CustomDataRowComparer());
            //var result2 = result.Any() ? result.CopyToDataTable() : null;
            //if (result2 != null && result2.Rows.Count > 0)
            //{
            //    foreach (DataRow item in result2.Rows)
            //    {
            //        sp = new List<SqlParameter>()
            //            {
            //                new SqlParameter() {ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value= item.ItemArray[0]},
            //                new SqlParameter() {ParameterName = "@hoteltype_name", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[1]},
            //                new SqlParameter() {ParameterName = "@hoteltype_name_de", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[2]}
            //            };
            //        ClsBase.SyncMasterList("DCP_Migration_List_HotelType", DCPConnectionstring, sp);
            //    }
            //}
        }

        public void MigrateRestaurantType(DataTable SourceTable)
        {
            try
            {
                IEnumerable<RestaurantTypeModel> sourceList = SourceTable.AsEnumerable().Select(row => new RestaurantTypeModel()
                {
                    id = Convert.ToInt32(row["smt_restauranttype_id"]),
                    restaurant_name = Convert.ToString(row["restauranttype_name"]),
                    restaurant_name_de = Convert.ToString(row["restauranttype_name_de"]),

                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migraterestauranttype", sourceList).Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("MigrateRestaurantType", ex);
            }
            

            //var result = SourceTable.AsEnumerable().Except(DestinationTable.AsEnumerable(), new CustomDataRowComparer());
            //var result2 = result.Any() ? result.CopyToDataTable() : null;
            //if (result2 != null && result2.Rows.Count > 0)
            //{
            //    foreach (DataRow item in result2.Rows)
            //    {
            //        sp = new List<SqlParameter>()
            //            {
            //                new SqlParameter() {ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value= item.ItemArray[0]},
            //                new SqlParameter() {ParameterName = "@restaurant_name", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[1]},
            //                new SqlParameter() {ParameterName = "@restaurant_name_de", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[2]}
            //            };
            //        ClsBase.SyncMasterList("DCP_Migration_List_restauranttype", DCPConnectionstring, sp);
            //    }
            //}
        }

        public void MigrateListLocationType(DataTable SourceTable)
        {
            try
            {
                IEnumerable<LocationTypeModel> sourceList = SourceTable.AsEnumerable().Select(row => new LocationTypeModel()
                {
                    id = Convert.ToInt32(row["smt_locationtype_id"]),
                    location_name = Convert.ToString(row["locationtype_name"]),
                    location_name_de = Convert.ToString(row["locationtype_name_de"]),

                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migratelocationtype", sourceList).Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("MigrateListLocationType", ex);
            }

            

            //var result = SourceTable.AsEnumerable().Except(DestinationTable.AsEnumerable(), new CustomDataRowComparer());
            //var result2 = result.Any() ? result.CopyToDataTable() : null;
            //if (result2 != null && result2.Rows.Count > 0)
            //{
            //    foreach (DataRow item in result2.Rows)
            //    {
            //        sp = new List<SqlParameter>()
            //            {
            //                new SqlParameter() {ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value= item.ItemArray[0]},
            //                new SqlParameter() {ParameterName = "@location_name", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[1]},
            //                new SqlParameter() {ParameterName = "@location_name_de", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[2]}
            //            };
            //        ClsBase.SyncMasterList("DCP_Migration_list_locationtype", DCPConnectionstring, sp);
            //    }
            //}
        }

        public void MigrateSuitableFor(DataTable SourceTable)
        {
            try
            {
                IEnumerable<SuitableForModel> sourceList = SourceTable.AsEnumerable().Select(row => new SuitableForModel()
                {
                    id = Convert.ToInt32(row["smt_suitablefor_id"]),
                    suitablefor_name = Convert.ToString(row["suitablefor_name"]),
                    suitablefor_name_de = Convert.ToString(row["suitablefor_name_de"]),
                    suitablefor_type = Convert.ToInt32(row["smt_suitablefor_type"])
                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/migratesuitablefor", sourceList).Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("MigrateSuitableFor", ex);
            }

            

            //var result = SourceTable.AsEnumerable().Except(DestinationTable.AsEnumerable(), new CustomDataRowComparer());
            //var result2 = result.Any() ? result.CopyToDataTable() : null;
            //if (result2 != null && result2.Rows.Count > 0)
            //{
            //    foreach (DataRow item in result2.Rows)
            //    {
            //        sp = new List<SqlParameter>()
            //            {
            //                new SqlParameter() {ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value= item.ItemArray[0]},
            //                new SqlParameter() {ParameterName = "@suitablefor_name", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[1]},
            //                new SqlParameter() {ParameterName = "@suitablefor_name_de", SqlDbType = SqlDbType.NVarChar, Value= item.ItemArray[2]},
            //                new SqlParameter() {ParameterName = "@suitablefor_type", SqlDbType = SqlDbType.Int, Value= item.ItemArray[3]}

            //            };
            //        ClsBase.SyncMasterList("DCP_Migration_list_suitablefor", DCPConnectionstring, sp);
            //    }
            //}
        }


        public bool UpdateResourceStatus(int ResourceId,string ResourceType)
        {
            try
            {
                ClsBase clsBase = new ClsBase();
                return clsBase.UpdateResourceStatus(ResourceId, ResourceType);
            }
            catch(Exception ex)
            {
                Log.WriteToLogFile("UpdateResourceStatus", ex);
                return false;
            }
        }

        public DataSet DeleteKbResource()
        {
            DataSet _dataset = new DataSet();
           
            try
            {
                ClsBase = new ClsBase();
                _dataset = ClsBase.GetKbStatus(MarbetConnectionString);
                if(_dataset!=null && _dataset.Tables.Count>0 && _dataset.Tables[0].Rows.Count>0)
                {
                    DeleteKbResourceFromPortal(_dataset.Tables[0]);
                    ClsBase.UpdateKbStatusRecord(_dataset.Tables[0]);
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("DeleteKbResource", ex);
                //throw ex;
            }
            return _dataset;
        }

        private void DeleteKbResourceFromPortal(DataTable SourceTable)
        {
            try
            {
                IEnumerable<KbStatusModel> sourceList = SourceTable.AsEnumerable().Select(row => new KbStatusModel()
                {
                    id = Convert.ToInt32(row["id"]),
                    kb_id = Convert.ToInt32(row["kb_id"]),
                    kb_type = Convert.ToInt32(row["kb_type"]),
                    dcp_status = Convert.ToString(row["dcp_status"]),
                    update_from = Convert.ToString(row["update_from"])
                }).ToList();


                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(APIUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",Common.Token);
                    HttpResponseMessage responseMessage = httpClient.PostAsJsonAsync("api/sync/deleteresource", sourceList).Result;

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        // Console.Write("success");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteToLogFile("DeleteKbResourceFromPortal", ex);
               // throw;
            }
        }

        
    }

    public class CustomDataRowComparer : IEqualityComparer<DataRow>
    {
        public bool Equals(DataRow x, DataRow y)
        {
            for (int i = 0; i < x.Table.Columns.Count; i++)
            {
                if (x[i].ToString() != y[i].ToString())
                {
                    return false;
                }

            }
            return true;
        }

        public int GetHashCode(DataRow obj)
        {
            return obj.ToString().GetHashCode();
        }
    }


   
}
