﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
namespace DCPKBSyncBAL
{
    public static class clsConvertToJson
    {
        public static string ConvertToJson(DataSet sourceDataset)
        {
            return JsonConvert.SerializeObject(sourceDataset);
        }
        public static string ConvertToJson(string jsonString)
        {
            return JsonConvert.SerializeObject(jsonString);
        }
    }
}
