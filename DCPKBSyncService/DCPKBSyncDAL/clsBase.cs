﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using DCPKBSyncUtility;
namespace DCPKBSyncDAL
{
    public class ClsBase
    {
        private string MarbetConnectionString { get => ConfigurationSettings.AppSettings["MarbetConnectionString"].ToString(); }

        

        public DataSet GetMasterList(string SPName,string ConnectionString)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlCommand command;
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    command = new SqlCommand(SPName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    var dataadaptor = new SqlDataAdapter(command);
                    dataadaptor.Fill(dataSet);
                    if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables.Count == 7)
                    {
                        dataSet.Tables[0].TableName = "list_airport";
                        dataSet.Tables[1].TableName = "list_country";
                        dataSet.Tables[2].TableName = "list_hoteltype";
                        dataSet.Tables[3].TableName = "list_restauranttype";
                        dataSet.Tables[4].TableName = "list_locationtype";
                        dataSet.Tables[5].TableName = "list_kitchen";
                        dataSet.Tables[6].TableName = "list_suitablefor";
                    }
                }
            }
            catch(Exception ex)
            {
                Log.WriteToLogFile("GetMasterList", ex);
            }       
            return dataSet;
        }

        public DataSet GetResourceList(string SPName, string ConnectionString,string ResourceType)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlCommand command;
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    command = new SqlCommand(SPName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    var dataadaptor = new SqlDataAdapter(command);
                    dataadaptor.Fill(dataSet);
                    if(ResourceType == "Hotels")
                    {
                        if (dataSet != null && dataSet.Tables.Count > 0)
                        {
                            dataSet.Tables[0].TableName = ResourceType;
                            dataSet.Tables[1].TableName = "HotelType";
                            dataSet.Tables[2].TableName = "Photos";
                        }
                    }
                    if(ResourceType == "Locations")
                    {
                        if (dataSet != null && dataSet.Tables.Count > 0)
                        {
                            dataSet.Tables[0].TableName = ResourceType;
                            dataSet.Tables[1].TableName = "LocationType";
                            dataSet.Tables[2].TableName = "KbSuitable";
                            dataSet.Tables[3].TableName = "Photos";
                        }
                    }
                    if(ResourceType == "Restaurants")
                    {
                        if (dataSet != null && dataSet.Tables.Count > 0)
                        {
                            dataSet.Tables[0].TableName = ResourceType;
                            dataSet.Tables[1].TableName = "RestaurantTType";
                            dataSet.Tables[2].TableName = "Kitchens";
                            dataSet.Tables[3].TableName = "KbSuitable";
                            dataSet.Tables[4].TableName = "Photos";
                        }
                    }
                }
            }
            catch(Exception ex)
            {
               Log.WriteToLogFile("GetResourceList", ex);
            }
            return dataSet;
        }

        public void SyncMasterList(string SPName, string ConnectionString, List<SqlParameter> parameter)
        {
            try
            {
                SqlCommand command;
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    command = new SqlCommand(SPName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    
                        command.Parameters.AddRange(parameter.ToArray());
                    //}
                    //command.Parameters.AddWithValue(parameter.ParameterName, parameter.Value);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch(Exception ex)
            {
               Log.WriteToLogFile("SyncMasterList", ex);
            }
        }

        public bool UpdateResourceStatus(int ResourceId, string ResourceType)
        {
            try
            {
                SqlCommand command;
                using (var connection = new SqlConnection(MarbetConnectionString))
                {
                    connection.Open();
                    command = new SqlCommand("DCP_Migration_UpdateResource", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Id", ResourceId);
                    command.Parameters.AddWithValue("@ResType", ResourceType);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                return true;
            }
            catch(Exception ex)
            {
                Log.WriteToLogFile("UpdateResourceStatus", ex);
                return false;
            }
        }

        public DataSet GetKbStatus(string ConnectionString)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlCommand command;
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    command = new SqlCommand("GetKbStatus", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    var dataadaptor = new SqlDataAdapter(command);
                    dataadaptor.Fill(dataSet);                    
                }
            }
            catch(Exception ex)
            {
               Log.WriteToLogFile("GetKbStatus", ex);
            }
            return dataSet;
        }

        public bool UpdateKbStatusRecord(DataTable dataTable)
        {
            bool flag = false;
            try
            {
                string commaSeparatedValue = string.Join(",", dataTable.AsEnumerable().Select(t => t.Field<int>("id").ToString()).ToArray());
                SqlCommand command;
                using (var connection = new SqlConnection(MarbetConnectionString))
                {
                    connection.Open();
                    command = new SqlCommand("UpdateKbStatus", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CommaSeparatedIds", commaSeparatedValue);
                    command.Parameters.AddWithValue("@ModifyBy", 1);
                    command.Parameters.AddWithValue("@CurrDate", DateTime.Now);
                    command.ExecuteNonQuery();
                }
                flag = true;
            }
            catch (Exception ex)
            {
               Log.WriteToLogFile("UpdateKbStatusRecord", ex);
            }
            return flag;
        }

        

    }
}
